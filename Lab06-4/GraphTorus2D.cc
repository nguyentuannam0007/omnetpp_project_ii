///*
// * GraphTorus2D.cc
// *
// *  Created on: May 25, 2020
// *      Author: Admin
// */
//#include<iostream>
//#include<vector>
//#include<queue>
//#include<stack>
//#include<fstream>
//#include<time.h>
//
//using namespace std;
//
//vector<vector<int> > adj;
//vector<pair<int, int> > pairOfHosts;
//int N;
//
//
//void buildADJ()
//{
//    // For horizontal connection
//    for (int i = 0; i < N; i++){
//        for (int j = 0; j < N - 1; j++){
//            // Switch[j] <--> Switch[j+1]
//            adj[i*N + j].push_back(i*N + j+1);
//            adj[i*N + j+1].push_back(i*N+ j);
//        }
//    }
//
//    // For vertical connection
//    for (int i = 0; i < N - 1; i++){
//        for (int j = 0; j < N; j++){
//            adj[i*N + j].push_back((i+1)*N + j);
//            adj[(i+1)*N + j].push_back(i*N + j);
//        }
//    }
//
//    // For begin and end in vertical
//    for (int i = 0; i < N; i++){
//        adj[i].push_back(N*(N-1) + i);
//        adj[N*(N-1) + i].push_back(i);
//    }
//    // For begin and end in horizontal
//    for (int i = 0; i < N; i++){
//        adj[i*N].push_back(i*N + N - 1);
//        adj[i*N + N - 1].push_back(i*N);
//    }
//
//}
//
//
//
//int findPath(int source, int dest)
//{
//    // Return -1 if source == dest
//    // Or next id node
//
//    int next = -1;
//
//    if (source == dest) return -1;
//
//    int prev[N*N];
//    bool isVisted[N*N];
//
//    for (int i = 0; i < N*N ; i++) isVisted[i] = false;
//
//    queue<int> myqueue;
//
//    myqueue.push(source);
//    isVisted[source] = true;
//    prev[source] = -1;
//
//    bool existsPath = false;
//    int front = -1;
//
//    while(!myqueue.empty())
//    {
//        front = myqueue.front();
//        myqueue.pop();
//
//        if (front == dest){
//            existsPath = true;
//            break;
//        }
//
//        for (int i = 0; i < adj[front].size(); i++)
//        {
//            int v = adj[front][i];
//            if (!isVisted[v]){
//                prev[v] = front;
//                isVisted[v] = true;
//                myqueue.push(v);
//            }
//        }
//    }
//
//    if (existsPath){
//
//        stack<int> mystack;
//        int v = dest;
//        while(prev[v] != source){
//            mystack.push(v);
//            v = prev[v];
//        }
//        return v;
//    }
//
//    return -2; // -2 if not exists path
//}
//
//void printADJ(string namefile)
//{
//    ofstream myfile;
//    myfile.open(namefile);
//
//    if (myfile.is_open()){
//        myfile << N*N << "\n";
//        for (int i = 0; i < N*N; i++){
//            myfile << i << " ";
//            for (int j = 0; j < adj[i].size(); j++){
//                myfile << adj[i][j] << " ";
//            }
//            myfile << "\n";
//        }
//
//        myfile.close();
//
//    }
//}
//
//void buildPairOfHost()
//{
//    if (N % 2 == 1) return;
//    int numHost = N*N;
//    vector<int> hosts;
//    for (int i = 0; i < numHost; i++){
//        hosts.push_back(i);
//    }
//
//    while(hosts.size() != 0){
//        int indexRandom = rand()%(hosts.size());
//
//        int source = hosts[indexRandom];
//        hosts.erase(hosts.begin() + indexRandom);
//
//        indexRandom = rand()%(hosts.size());
//
//        int dest = hosts[indexRandom];
//        hosts.erase(hosts.begin() + indexRandom);
//
//        pairOfHosts.push_back(make_pair(source, dest));
//    }
//}
//
//void printPairOfHost(string namefile)
//{
//    if (N % 2 == 1) return;
//    ofstream myfile;
//    myfile.open(namefile);
//
//    if(myfile.is_open()){
//        myfile << N*N / 2 << "\n";
//        for (int i = 0; i < pairOfHosts.size(); i++){
//                myfile << pairOfHosts[i].first << " " << pairOfHosts[i].second << "\n";
//        }
//        myfile.close();
//    }
//}
//
//void calThroughPut(){
//    double sizeOfMsg = 100000;
//    double datarates = 1000000000;
//    double intervalTime = 0.00001;
//
//    ifstream myfile;
//    myfile.open("pairOfHost.txt");
//    int N;
//    vector<int> dests;
//    if (myfile.is_open()){
//        myfile >> N;
//        for (int i = 0; i < N; i++){
//            int source, dest;
//            myfile >> source >> dest;
//            dests.push_back(dest);
//        }
//
//        myfile.close();
//    }
//
//    vector<vector<double> > hostWInterval(dests.size());
//
//    for (int i = 0; i < dests.size(); i++){
//        ifstream myfile;
//        string nameFile = "host-intervals" + to_string(dests[i]) + ".txt";
//        myfile.open(nameFile);
//        if (myfile.is_open()){
//            int numOfInterval, numOfMsg;
//            myfile >> numOfInterval;
//            for (int j = 0; j < numOfInterval; j++){
//                myfile >> numOfMsg;
//                hostWInterval[i].push_back(numOfMsg);
//            }
//            myfile.close();
//        }
//    }
//
//    vector<double> test;
//    for (int i = 0; i < hostWInterval[0].size(); i++){
//        double numOfMsg = 0;
//        for (int j = 0; j < hostWInterval.size(); j++){
//            numOfMsg += hostWInterval[j][i];
//        }
//        test.push_back(numOfMsg);
//    }
//
//    ofstream testFile;
//    testFile.open("test.txt");
//    if(testFile.is_open()){
//        for (int i = 0; i < test.size(); i++){
//            testFile << test[i] << "\n";
//        }
//        testFile.close();
//    }
//
//    for (int i = 0; i < hostWInterval.size(); i++){
//        for (int j = 0; j < hostWInterval[i].size(); j++){
//            hostWInterval[i][j] = hostWInterval[i][j] * sizeOfMsg / intervalTime;
//        }
//    }
//
//    vector<double> throughPuts;
//    for (int i = 0; i < hostWInterval[0].size(); i++){
//        double throughput = 0;
//        for (int j = 0; j < hostWInterval.size(); j++){
//            throughput += hostWInterval[j][i];
//        }
//        throughput /= dests.size();
//        throughput /= datarates;
//        throughput *= 100;
//        throughPuts.push_back(throughput);
//    }
//
//    double ans = 0;
//    for (int i = 0; i < throughPuts.size(); i++){
//        ans += throughPuts[i];
//    }
//
//    cout << ans / throughPuts.size() << "\n";
//
//    ofstream throughPutFile;
//    throughPutFile.open("throughtPut.txt");
//    if (throughPutFile.is_open()){
//        throughPutFile << throughPuts.size() << "\n";
//        for (int i = 0; i < throughPuts.size(); i++){
//            throughPutFile << throughPuts[i] << "\n";
//        }
//    }
//
//}
//
//int main()
//{
//
//    cin >> N;
//
//    for (int i = 0; i < N*N; i++){
//        vector<int> emptyVec;
//        adj.push_back(emptyVec);
//    }
//
//    srand(time(NULL));
//
//    buildADJ();
//    printADJ("torus2d.txt");
//
//    buildPairOfHost();
//    printPairOfHost("pairOfHost.txt");
//
////    calThroughPut();
//}
//
