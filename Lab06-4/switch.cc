/*
 * switch.cc
 *
 *  Created on: May 14, 2020
 *      Author: Admin
 */
#include<omnetpp.h>
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<list>
#include<vector>
#include<fstream>
#include<queue>
#include<stack>
#include<myMessage_m.h>
#include<switch_m.h>
#include<messageSignalFreeConn_m.h>
#include<delaySwitch_m.h>

using namespace omnetpp;
using namespace std;


class Switch : public cSimpleModule
{
private:
    vector<vector<int> > adj;
    vector<list<pair<HostMessage *, int>>> enb;
    vector<list<pair<HostMessage *, int>>> exb;

    vector<int> avaiExb;
    vector<int> counters; // Kiểm tra số gói tin có thể gửi đến các switch khác kết nối với nó
    vector<bool> isBusyChannel;

    map<int, int> routingTable;
    map<int, int> mapIndexToPort;

    int indexSwitch;
    int N; // Num of switch in torus2d;
    int sizeofEXB;
    int sizeofK;
    double intervalTime;
    double periodTime;
    double timeDelayMsg;
    double simulationTime;
    double creditDelayTime;

    SwitchMessage *transportMsg;
protected:
    void initialize() override;
    void handleMessage(cMessage *msg) override;
    void finish() override;
    SwitchMessage* generateTransportMsg(int indexEnbTrans, int indexExbTrans);
    SwitchMessage* generateForwardMsg(int indexExb);
    void forwardMessage(HostMessage *msg, int indexExb);
    void transportMessage(int indexEnb, int indexExb);
    void buildAdjFromFile(string namefile);
    void buildMapIndexToPort();
    void checkFrontOfEnb(int indexEnb);
    int findPath(int source, int dest);
};

Define_Module(Switch);

void Switch::initialize()
{
    indexSwitch = getIndex();
    buildAdjFromFile("torus2d.txt");
    buildMapIndexToPort();

    EV << "-1" << mapIndexToPort[-1] << "\n";

    sizeofEXB = (int) getParentModule()->par("sizeofEXB");
    sizeofK = (int) getParentModule()->par("sizeofNode");
    periodTime = (double) getParentModule()->par("periodTimeSwitch") / 1000;
    timeDelayMsg = (double) gate("gate$o", 0)->getChannel()->par("delay");
    creditDelayTime = (double) getParentModule()->par("creditDelayTime") / 1000;
    simulationTime = (double) getParentModule()->par("simulationTime") / 1000;


    for (int i = 0; i < sizeofK; i++){
        list<pair<HostMessage *, int> > entranceBuffer;
        enb.push_back(entranceBuffer);

        list<pair<HostMessage *, int> > exitBuffer;
        exb.push_back(exitBuffer);
        avaiExb.push_back(sizeofEXB);

        isBusyChannel.push_back(false);
        counters.push_back(sizeofEXB);
    }

}

void Switch::handleMessage(cMessage *msg)
{
    if (simTime().dbl() > simulationTime) return;

    if (SignalFreeConnMess *conFreeMsg = dynamic_cast<SignalFreeConnMess *>(msg)){
        // Khi một nút khác gửi lại tín hiệu đường truyền đã rảnh.
        // Kiểm tra gói tin trong exit buffer xem có gói tin nào muốn chuyển ra cổng hay không
        // Gửi lại một gói tin để sau một khoảng thời gian chu kỳ switch thì gửi đi
        int index = conFreeMsg->getArrivalGate()->getIndex();
        isBusyChannel[index] = false;

        DelayMessSwitch* delayMessSwitch = new DelayMessSwitch();
        delayMessSwitch->setPortNumber(index);
        scheduleAt(simTime().dbl() + periodTime, delayMessSwitch);

        delete conFreeMsg;
    }else if (HostMessage *hostmsg = dynamic_cast<HostMessage *>(msg)){
        // Đây là một gói tin của nút host
        // Entrance buffer luôn được đảm bảo là đủ không gian (với số cho trước) nhờ vào counter của nút gửi đến nó
        EV <<"Nhan duoc goi tin cua nut host" << "\n";

        int indexInGate = hostmsg->getArrivalGate()->getIndex();
        int next = findPath(indexSwitch, hostmsg->getDest());
        enb[indexInGate].push_back(make_pair(hostmsg, next)); // Insert to entrance buffer
        // Lấy thằng Mess đầu tiên của cổng này

        checkFrontOfEnb(indexInGate);

        // Gửi lại một gói tin rằng đường truyền đang rảnh có thể truyền đi ngay
        SignalFreeConnMess *freeConnMsg = new SignalFreeConnMess();
        send(freeConnMsg, "gateCheckConnFree$o", indexInGate);

    }else if (SwitchMessage *smsg = dynamic_cast<SwitchMessage *>(msg)){

        if (smsg->getIsTransportMsg()){
            transportMessage(smsg->getIndexEnbTrans(), smsg->getIndexExbTrans());
            delete smsg;
        }

    }else if (DelayMessSwitch *delayMess = dynamic_cast<DelayMessSwitch *>(msg)) {
        // Đây là tin nhắn trễ 1 interval của switch trên cổng có id của trong gói tin
        int indexExb = delayMess->getPortNumber();
        if (exb[indexExb].size() > 0){
            forwardMessage(exb[indexExb].front().first, indexExb);
        }
        delete delayMess;
    }else {
        // Đây là tin nhẵn mang nhãn là CREDIT_DELAY_TIME

        int indexPort = msg->getArrivalGate()->getIndex();
        counters[indexPort] += 1;
        if (exb[indexPort].size() > 0){ // Kiểm tra xem có gói tin nào trong exit buffer muốn gửi đi không
           forwardMessage(exb[indexPort].front().first, indexPort);
        }
        delete msg;
    }
}

void Switch::forwardMessage(HostMessage *msg, int indexExb)
{
    // Kiểm tra đường truyền có bận hay không để chuyển ra cổng đích.
    if (!isBusyChannel[indexExb] && exb[indexExb].size() > 0 && counters[indexExb] > 0){
        send(msg, "gate$o", indexExb);
        isBusyChannel[indexExb] = true;
        avaiExb[indexExb]++;
        exb[indexExb].pop_front();
        if (indexExb != sizeofK - 1)
            counters[indexExb] -= 1;


        int indexEnbCanTransport = -1;
        int idMsgCanTransport = numeric_limits<int>::max();
        for (int i = 0; i < enb.size(); i++){
            if (enb[i].size() > 0){
                if (mapIndexToPort[enb[i].front().second] == indexExb && enb[i].front().first->getId() < idMsgCanTransport){
                    idMsgCanTransport = enb[i].front().first->getId();
                    indexEnbCanTransport = i;
                }
            }
        }

        if (indexEnbCanTransport != -1){
            avaiExb[indexExb]--;
            SwitchMessage *smsg = generateTransportMsg(indexEnbCanTransport, indexExb);
            EV << "IndexEnbCanTransport: " << indexEnbCanTransport << "\n";
            scheduleAt(simTime().dbl() + periodTime, smsg);
        }
    }
}

void Switch::transportMessage(int indexEnb, int indexExb)
{
    if(exb[indexExb].size() < sizeofEXB && enb[indexEnb].size() > 0){
        exb[indexExb].push_back(enb[indexEnb].front());
        enb[indexEnb].pop_front();

        EV << "Chuyen goi tin tu " << indexEnb << " sang " << indexExb << "\n";
        // Gói tin này dùng để tự gói cho chính switch hẹn sau khoảng thời gian này delaySwitch sẽ kiểm tra vào forward gói tin ra cổng đích
        DelayMessSwitch* delayMessSwitch = new DelayMessSwitch();
        delayMessSwitch->setPortNumber(indexExb);
        scheduleAt(simTime().dbl() + periodTime, delayMessSwitch);

        // Đồng thời gửi lại một gói tin thông báo cho nút kết nối với entrance buffer này rằng nó đã có 1 chỗ trống mới (Tăng counter lên)
        cMessage *msg = new cMessage("CREDIT_DELAY_TIME");
        send(msg, "gateSignalCreditDelay$o", indexEnb);

        checkFrontOfEnb(indexEnb); // Kiểm tra xem entrance buffer thứ indexEnb của switch có gói tin ở đầu hay không để chuyển sang exb tương ứng
    }
}

void Switch::checkFrontOfEnb(int indexEnb)
{
    if (enb[indexEnb].size() > 0){

        pair<HostMessage *, int> frontMess =  enb[indexEnb].front();
        int indexExbTrans = mapIndexToPort[frontMess.second];
        // Kiểm tra xem các entrance buffer khác có message nào với id bé hơn mà muốn ra cổng exb cùng với nó hay không

        int indexEnbCanForwardMsg = indexEnb;
        int minIndexId = frontMess.first->getId();
        for (int i = 0; i < sizeofK - 1; i++){
            if (enb[i].size() > 0){
                pair<HostMessage *, int> frontMsg = enb[i].front();
                if (frontMsg.second == frontMess.second && frontMsg.first->getId() < minIndexId){
                    indexEnbCanForwardMsg = i;
                    minIndexId = frontMsg.first->getId();
                }
            }
        }

        if(avaiExb[indexExbTrans] > 0){ // check if exit buffer is full or not
            avaiExb[indexExbTrans]--;
            SwitchMessage *smsg = generateTransportMsg(indexEnbCanForwardMsg, indexExbTrans);
            scheduleAt(simTime().dbl() + periodTime, smsg);
        }
    }
}

SwitchMessage* Switch::generateTransportMsg(int indexEnbTrans, int indexExbTrans)
{
    SwitchMessage *smsg = new SwitchMessage();
    smsg->setIndexEnbTrans(indexEnbTrans);
    smsg->setIndexExbTrans(indexExbTrans);

    smsg->setIsTransportMsg(true);
    smsg->setIsForwardMsg(false);
    smsg->setIndexExbForward(-1);
    return smsg;
}

SwitchMessage* Switch::generateForwardMsg(int indexExb)
{
    SwitchMessage *msg = new SwitchMessage();
    msg->setIndexEnbTrans(-1);
    msg->setIndexExbTrans(-1);
    msg->setIsTransportMsg(false);
    msg->setIsForwardMsg(true);
    msg->setIndexExbForward(indexExb);

    return msg;
}

void Switch::buildAdjFromFile(string namefile)
{
    ifstream torus2dfile;
    torus2dfile.open(namefile);
    string line;

    if (torus2dfile.is_open()){
        torus2dfile >> N;
        int from;
        for (int i = 0; i < N; i++){
            torus2dfile >> from;
            vector<int> adjfrom;
            int to;
            for (int j = 1; j <= 4; j++){
                torus2dfile >> to;
                adjfrom.push_back(to);
            }
            adj.push_back(adjfrom);
        }
        torus2dfile.close();
    }
}

int Switch::findPath(int source, int dest)
{
    // Return -1 if source == dest
    // Or next id node

    int next = -1;

    if (source == dest) return -1;

    int prev[N*N];
    bool isVisted[N*N];

    for (int i = 0; i < N*N ; i++) isVisted[i] = false;

    queue<int> myqueue;

    myqueue.push(source);
    isVisted[source] = true;
    prev[source] = -1;

    bool existsPath = false;
    int front = -1;

    while(!myqueue.empty())
    {
        front = myqueue.front();
        myqueue.pop();

        if (front == dest){
            existsPath = true;
            break;
        }

        for (int i = 0; i < adj[front].size(); i++)
        {
            int v = adj[front][i];
            if (!isVisted[v]){
                prev[v] = front;
                isVisted[v] = true;
                myqueue.push(v);
            }
        }
    }

    if (existsPath){

        stack<int> mystack;
        int v = dest;
        while(prev[v] != source){
            mystack.push(v);
            v = prev[v];
        }
        return v;
    }

    return -2; // -2 if not exists path
}

void Switch::buildMapIndexToPort()
{
    int indexPort = 0;
    for (int i = 0; i < adj[indexSwitch].size(); i++){
        mapIndexToPort[adj[indexSwitch][i]] = indexPort++;
    }

    mapIndexToPort[-1] = indexPort; // Nếu tìm được next = -1 thì cổng ra chính là Host đang kết nối với Switch
}


void Switch::finish()
{

}


