/*
 * host.cc
 *
 *  Created on: May 7, 2020
 *      Author: Admin
 */
#include<omnetpp.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include "myMessage_m.h"
#include "messageSignalFreeConn_m.h"
using namespace std;
using namespace omnetpp;




// Source queue
struct SQ {
    int idHead = 0;
    int idTail = 0;

    int getHeader(){
        int ans = -1;
        if (idHead > 0){
            ans = idHead;
            if (idTail == 0){
                idHead = 0;
            }else{
                idHead++;
                if (idHead == idTail)
                    idTail = 0;
            }
        }
        return ans;
    }
};

// Exit buffer
struct EB {
    int *exitBuff;
    int avaiLength;
};




class Host: public cSimpleModule {
private:
    int indexHost;
    int source = -1;
    int dest = -1;
    int sizeOfEXB;
    // Source queue and Exit buffer in here
    SQ sq; // Source queue
    EB eb; // Exit buffer

    int counter_msg = 0;
    int counter = 0;

    int counter_msg_send = 0;
    // For check channel is busy or not
    bool isBusyChannel = false;

    int *msgForInterval;   // For destination node: count message in interval times;
    int lengthOfMsgForInterval;
    double timeForGenMess; // For source node: time for generate message
    double timeSimulation, intervalTime; // Time for simulation
    double timeDelayMsg = 0;
    double lastTimeSendMsg;

    HostMessage * genMsg;
    HostMessage * wakeUpMsg;
protected:
    void handleMessage(cMessage *msg) override;
    void initialize() override;
    void finish() override;
    HostMessage* generateMessage(int idMsg);

    void insertToSQ(int idMsg);
    void insertToEB();
    void sendMessageInEB();
    void buildPairOfHost(string namefile);
};

Define_Module(Host);

void Host::initialize()
{
    indexHost = getIndex();
    buildPairOfHost("pairOfHost.txt");

//    if (source == indexHost){
//        EV << "I am source node: " << source << " to " << dest << "\n";
//    }else{
//        EV << "I am destination node: " << source << " to " << dest << "\n";
//    }

    // Get simulation's time
    timeSimulation = (double) getParentModule()->par("simulationTime") / 1000;
    intervalTime = (double) getParentModule()->par("intervalTime") / 1000;
    timeDelayMsg = (double) gate("gate$o")->getChannel()->par("delay");
    EV << timeDelayMsg << "\n";
    sizeOfEXB = (int) getParentModule()->par("sizeofEXB");
    counter = sizeOfEXB;
    eb.avaiLength = counter;
    isBusyChannel = false;

    if (dest == indexHost){ // For destination node
        lengthOfMsgForInterval = (int)(timeSimulation / intervalTime);
        msgForInterval = (int *)calloc(lengthOfMsgForInterval, sizeof(int));
    }else { // For source node
        eb.exitBuff = (int *)calloc(eb.avaiLength, sizeof(int));
        timeForGenMess = (double)getParentModule()->par("cycleTimeGenMsg") / 1000;

        wakeUpMsg = new HostMessage();
        wakeUpMsg->setId(-1);

        insertToSQ(++counter_msg); // Generate a message

        // Generate a message for generate message after cycle generate message
        genMsg = new HostMessage();
        genMsg->setId(-1);
        scheduleAt(simTime().dbl() + timeForGenMess, genMsg);
    }
}

void Host::handleMessage(cMessage *msg)
{
    if (simTime().dbl() > timeSimulation) return;

    if(strcmp(msg->getName(), "CREDIT_DELAY_TIME") == 0){ // Signal from switch notify that entrance buffer had move a msg
        counter++;
        delete msg;
    }else if (HostMessage * hostMsg = dynamic_cast<HostMessage *>(msg)){
        if (hostMsg == genMsg){ // Generate a new message
            insertToSQ(++counter_msg);
            scheduleAt(timeForGenMess + simTime().dbl(), hostMsg);
        }else {
            if (indexHost == hostMsg->getDest()){ // Is destination;
               int indexTimeInterval = (simTime().dbl() / intervalTime - 1.0);
               msgForInterval[indexTimeInterval] += 1;
               delete hostMsg;

               SignalFreeConnMess *conFreeMsg = new SignalFreeConnMess();
               send(conFreeMsg, "connectionFreeGate$o");
            }
        }
    }else { // Đây là gói tin thông báo đường truyền rảnh
        isBusyChannel = false;
        sendMessageInEB();
        delete msg;
    }
}

HostMessage* Host::generateMessage(int idMsg)
{
    HostMessage* msg = new HostMessage();
    msg->setId(idMsg);
    msg->setDest(dest);
    msg->setSource(source);
    return msg;
}


void Host::insertToSQ(int idMsg)
{
    if (sq.idHead == 0){
        sq.idHead = idMsg;
    }else{
        sq.idTail = idMsg;
    }

    insertToEB();
}

void Host::insertToEB()
{

    while (eb.avaiLength > 0){
        int idMsg = sq.getHeader();
        if (idMsg > 0){
            eb.exitBuff[sizeOfEXB - eb.avaiLength] = idMsg;
            eb.avaiLength--;
        }else {
            break;
        }
    }

    sendMessageInEB();
}

void Host::sendMessageInEB()
{
    if (counter > 0 && eb.avaiLength < sizeOfEXB){ // Check if exists any message in Exit buffer
        if (!isBusyChannel){
            // Listener channel: is busy or not
            isBusyChannel = true;
            counter--;

            counter_msg_send++;
            lastTimeSendMsg = simTime().dbl();
            HostMessage *msg = generateMessage(eb.exitBuff[0]);
            msg->setBitLength(100000);
            send(msg, "gate$o");

            for (int i = 1; i < sizeOfEXB; i++){
                eb.exitBuff[i-1] = eb.exitBuff[i];
            }
            eb.exitBuff[sizeOfEXB - 1] = 0;
            eb.avaiLength++;
        }
    }
}


void Host::buildPairOfHost(string namefile)
{
    ifstream myfile;
    myfile.open(namefile);
    if(myfile.is_open()){
        int numPair = 0;
        myfile >> numPair;
        for (int i = 0; i < numPair; i++){
            int _source, _dest;
            myfile >> _source >> _dest;
            if (_source == indexHost || _dest == indexHost){
                source = _source;
                dest = _dest;
                break;
            }
        }
        myfile.close();
    }
}

void Host::finish()
{
    if (indexHost == dest)
    {
        int count_msg = 0;
        EV << "Msg from " << source << " to " << dest << "\n";
        for (int i = 0; i < lengthOfMsgForInterval; i++){
            if (msgForInterval[i] == 1)
            //    EV << "Interval Time " << (i + 1) << " : " << msgForInterval[i] << "\n";
            count_msg += msgForInterval[i];
        }
        EV << "Number of Msg: " << count_msg << "\n";
        EV << "Number of msg per intervalTime in simulation time: " << (double)count_msg * timeSimulation / lengthOfMsgForInterval << "\n";

        ofstream intervalFile;
        string nameFile = "host-intervals" + to_string(indexHost) + ".txt";
        intervalFile.open(nameFile);
        if (intervalFile.is_open()){
            intervalFile << lengthOfMsgForInterval << "\n";
            for (int i = 0; i < lengthOfMsgForInterval; i++){
                intervalFile << msgForInterval[i] << "\n";
            }
            intervalFile.close();
        }else {
            EV << "Cannot open file\n";
        }

    }else {
        EV << lastTimeSendMsg  << "\n";
        EV << "Dau exit buffer : " << eb.exitBuff[0] << "\n";
        EV << "SignConnFree : " << isBusyChannel << "\n";
        EV << "Counter: " << counter << "\n";
    }
}
