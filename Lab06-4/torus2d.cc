///*
// * torus2d.cc
// *
// *  Created on: May 25, 2020
// *      Author: Admin
// */
//
//
//#include<iostream>
//#include<fstream>
//
//using namespace std;
//
//int main()
//{
//    int N;
//    cin >> N;
//
//    ofstream myfile;
//    myfile.open("TORUS2D.ned");
//
//    myfile << "\n";
//    myfile << "\n";
//    myfile << "\n";
//    myfile << "network Torus2D\n";
//    myfile << "{\n";
//    myfile << "    parameters:\n";
//    myfile << "        int N = " << N ";\n";
//    myfile << "        double simulationTime @unit(ms);\n";
//    myfile << "        double intervalTime @unit(ms);\n";
//    myfile << "        double cycleTimeGenMsg @unit(ms);\n";
//    myfile << "        double periodTimeSwitch @unit(ms);\n";
//    myfile << "        double creditDelayTime @unit(ms);\n";
//    myfile << "        int sizeofEXB;\n";
//    myfile << "        int sizeofNode;\n";
//    myfile << "    types:\n";
//    myfile << "        channel DelayChannel extends ned.DatarateChannel\n";
//    myfile << "        {\n";
//    myfile << "            delay = 0.1ms;\n";
//    myfile << "            datarate = 1Gbps;\n";
//    myfile << "        }\n";
//    myfile << "        channel SignalCreditDelayChannel extends ned.DelayChannel {\n";
//    myfile << "         delay = creditDelayTime;\n";
//    myfile << "        }\n";
//    myfile << "        channel CheckConnFreeChannel extends ned.DelayChannel \n";
//    myfile << "        {\n";
//    myfile << "            parameters:\n";
//    myfile << "                @display(\"ls=,0\");\n";
//    myfile << "            delay = 0ms;\n";
//    myfile << "        }\n";
//    myfile << "    submodules:\n";
//    myfile << "        switch[N*N]: Switch {\n";
//    myfile << "            @display(\"p=,,m,$N,100,100\");\n";
//    myfile << "        }\n";
//    myfile << "        host[N*N]: Host;\n";
//    myfile << "    connections:\n";
//    myfile << "        for i = 0..N-1, for j=0..N-2 {\n";
//    myfile << "            switch[i*N + j].gate++ <--> DelayChannel <--> switch[i*N + j+1].gate++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-2, for j=0..N-1 {\n";
//    myfile << "            switch[i*N + j].gate++ <--> DelayChannel <--> switch[(i+1)*N + j].gate++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-1, if N > 2 {\n";
//    myfile << "            switch[i].gate++ <--> DelayChannel <--> switch[(N-1)*N + i].gate++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-1, if N > 2 {\n";
//    myfile << "            switch[i*N].gate++ <--> DelayChannel <--> switch[i*N + N - 1].gate++;\n";
//    myfile << "        }\n";
//    myfile << "     \n";
//    myfile << "     // For Host connect to Switch. 1Switch - 1Host\n";
//    myfile << "     for i = 0..N*N-1 {\n";
//    myfile << "         host[i].gate <--> DelayChannel <--> switch[i].gate++;\n";
//    myfile << "     }\n";
//    myfile << "     \n";
//    myfile << "     \n";
//    myfile << "     // For check connection free on OtherSwitch;\n";
//    myfile << "        for i = 0..N-1, for j=0..N-2 {\n";
//    myfile << "            switch[i*N + j].gateCheckConnFree++ <--> CheckConnFreeChannel <--> switch[i*N + j+1].gateCheckConnFree++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-2, for j=0..N-1 {\n";
//    myfile << "            switch[i*N + j].gateCheckConnFree++ <--> CheckConnFreeChannel <--> switch[(i+1)*N + j].gateCheckConnFree++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-1, if N > 2 {\n";
//    myfile << "            switch[i].gateCheckConnFree++ <--> CheckConnFreeChannel <--> switch[(N-1)*N + i].gateCheckConnFree++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-1, if N > 2 {\n";
//    myfile << "            switch[i*N].gateCheckConnFree++ <--> CheckConnFreeChannel <--> switch[i*N + N - 1].gateCheckConnFree++;\n";
//    myfile << "        }    \n";
//    myfile << "        \n";
//    myfile << "        // For check connection free on Host\n";
//    myfile << "        \n";
//    myfile << "        for i = 0..N*N - 1 {\n";
//    myfile << "             host[i].connectionFreeGate <--> CheckConnFreeChannel <--> switch[i].gateCheckConnFree++;\n";
//    myfile << "        }\n";
//    myfile << "        \n";
//    myfile << "        // For check signal on other switch -- CREDIT_DELAY_TIME on swtich when entrance buffer has a sit\n";
//    myfile << "        \n";
//    myfile << "        for i = 0..N-1, for j=0..N-2 {\n";
//    myfile << "            switch[i*N + j].gateSignalCreditDelay++ <--> SignalCreditDelayChannel <--> switch[i*N + j+1].gateSignalCreditDelay++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-2, for j=0..N-1 {\n";
//    myfile << "            switch[i*N + j].gateSignalCreditDelay++ <--> SignalCreditDelayChannel <--> switch[(i+1)*N + j].gateSignalCreditDelay++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-1, if N > 2 {\n";
//    myfile << "            switch[i].gateSignalCreditDelay++ <--> SignalCreditDelayChannel <--> switch[(N-1)*N + i].gateSignalCreditDelay++;\n";
//    myfile << "        }\n";
//    myfile << "\n";
//    myfile << "        for i = 0..N-1, if N > 2 {\n";
//    myfile << "            switch[i*N].gateSignalCreditDelay++ <--> SignalCreditDelayChannel <--> switch[i*N + N - 1].gateSignalCreditDelay++;\n";
//    myfile << "        }  \n";
//    myfile << "\n";
//    myfile << "        // For check signal on Host -- CREDIT_DELAY_TIME on switch when entrance buffer has a sit;\n";
//    myfile << "        for i = 0..N*N - 1 {\n";
//    myfile << "             host[i].signalGate <--> SignalCreditDelayChannel <--> switch[i].gateSignalCreditDelay++;\n";
//    myfile << "        }\n";
//    myfile << "}\n";
//
//
//
////    ifstream myfile;
////    ofstream outfile;
////
////    myfile.open("input.txt");
////    outfile.open("out.txt");
////    string line;
////    if (myfile.is_open()){
////        while(getline(myfile, line)){
////            outfile << "myfile << \"" << line << "\\n\";\n";
////        }
////
////        myfile.close();
////        outfile.close();
////    }
//
//    return 0;
//}
//
//
//
//
//
//
