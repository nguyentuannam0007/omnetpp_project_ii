///*
// * GraphTorus2D.cc
// *
// *  Created on: May 25, 2020
// *      Author: Admin
// */
//#include<iostream>
//#include<vector>
//#include<queue>
//#include<stack>
//
//using namespace std;
//
//vector<vector<int> > adj;
//int N;
//
//
//void buildADJ()
//{
//    // For horizontal connection
//    for (int i = 0; i < N; i++){
//        for (int j = 0; j < N - 1; j++){
//            // Switch[j] <--> Switch[j+1]
//            adj[i*N + j].push_back(i*N + j+1);
//            adj[i*N + j+1].push_back(i*N+ j);
//        }
//    }
//
//    // For vertical connection
//    for (int i = 0; i < N - 1; i++){
//        for (int j = 0; j < N; j++){
//            adj[i*N + j].push_back((i+1)*N + j);
//            adj[(i+1)*N + j].push_back(i*N + j);
//        }
//    }
//
//    // For begin and end in vertical
//    for (int i = 0; i < N; i++){
//        adj[i].push_back(N*(N-1) + i);
//        adj[N*(N-1) + i].push_back(i);
//    }
//    // For begin and end in horizontal
//    for (int i = 0; i < N; i++){
//        adj[i*N].push_back(i*N + N - 1);
//        adj[i*N + N - 1].push_back(i*N);
//    }
//
//}
//
//
//
//vector<int> findPath(int source, int dest)
//{
//    vector<int> path;
//    int prev[N*N];
//    bool isVisted[N*N];
//    for (int i = 0; i < N*N ; i++) isVisted[i] = false;
//
//    queue<int> myqueue;
//
//    myqueue.push(source);
//    isVisted[source] = true;
//    prev[source] = -1;
//
//    bool existsPath = false;
//    int front = -1;
//
//    while(!myqueue.empty())
//    {
//        front = myqueue.front();
//        myqueue.pop();
//
//        if (front == dest){
//            existsPath = true;
//            break;
//        }
//
//        for (int i = 0; i < adj[front].size(); i++)
//        {
//            int v = adj[front][i];
//            if (!isVisted[v]){
//                prev[v] = front;
//                isVisted[v] = true;
//                myqueue.push(v);
//            }
//        }
//    }
//
//    if (existsPath){
//
//        stack<int> mystack;
//        int v = prev[dest];
//        while(prev[v] != -1){
//            mystack.push(v);
//            v = prev[v];
//        }
//
//        while(!mystack.empty()){
//            path.push_back(mystack.top());
//            mystack.pop();
//        }
//    }
//
//    return path;
//}
//
//void printADJ()
//{
//    cout << "ADJ of Torus2D graph:\n";
//    for (int i = 0; i < N*N; i++){
//        cout << "Node " << i << " : ";
//        for (int j = 0; j < adj[i].size(); j++){
//            cout << adj[i][j] << " ";
//        }
//        cout << "\n";
//    }
//}
//
//int main()
//{
//
//    cin >> N;
//
//    for (int i = 0; i < N*N; i++){
//        vector<int> emptyVec;
//        adj.push_back(emptyVec);
//    }
//
//    buildADJ();
//
//    printADJ();
//
//    int source, dest;
//    while(true)
//    {
//        cin >> source >> dest;
//        if (source == 0 && dest == 0){
//            break;
//        }
//        vector<int> path = findPath(source, dest);
//        cout << "Path from " << source << " to " << dest << " : ";
//        for (int i = 0; i < path.size(); i++){
//            cout << path[i] << " ";
//        }
//        cout << "\n";
//    }
//
//}
//
//
//
//
