/*
 * switch.cc
 *
 *  Created on: May 14, 2020
 *      Author: Admin
 */
#include<omnetpp.h>
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<list>
#include<vector>
#include<myMessage_m.h>
#include<switch_m.h>

using namespace omnetpp;
using namespace std;


class Switch : public cSimpleModule
{
private:
    vector<list<HostMessage *>> enb;
    vector<list<HostMessage *>> exb;

    int avaiDesExb; // Need to change
    int sizeofEXB;
    int sizeofK;
    double intervalTime;
    double periodTime;
    double timeDelayMsg;
    double simulationTime;
    double creditDelayTime;
    bool isBusyChannel;

    SwitchMessage *transportMsg;
    SwitchMessage* wakeUp; // This is a message for notifying Channel is not busy after delayTime in Channel
    SwitchMessage* delaySwitchMess;
protected:
    void initialize() override;
    void handleMessage(cMessage *msg) override;
    void finish() override;
    SwitchMessage* generateTransportMsg(int indexEnbTrans);
    SwitchMessage* generateForwardMsg(int indexExb);
    void forwardMessage(HostMessage *msg, int indexExb);
    void transportMessage(int indexEnb, int indexExb);
};

Define_Module(Switch);

void Switch::initialize()
{
    sizeofEXB = (int) getParentModule()->par("sizeofEXB");
    avaiDesExb = sizeofEXB;
    sizeofK = (int) getParentModule()->par("sizeofNode");
    periodTime = (double) getParentModule()->par("periodTimeSwitch") / 1000;
    timeDelayMsg = (double) gate("gate$o", 3)->getChannel()->par("delay");
    creditDelayTime = (double) getParentModule()->par("creditDelayTime") / 1000;
    simulationTime = (double) getParentModule()->par("simulationTime") / 1000;

    isBusyChannel = false;

    wakeUp = new SwitchMessage();
    delaySwitchMess = new SwitchMessage();

    for (int i = 0; i < sizeofK; i++){
        list<HostMessage *> entranceBuffer;
        enb.push_back(entranceBuffer);

        list<HostMessage *> exitBuffer;
        exb.push_back(exitBuffer);
    }


}

void Switch::handleMessage(cMessage *msg)
{
    if (simTime().dbl() > simulationTime) return;

    if (HostMessage *hostmsg = dynamic_cast<HostMessage *>(msg)){

        if (hostmsg->getSignConnFree() == true){
            isBusyChannel = false;
            scheduleAt(simTime().dbl() + periodTime, delaySwitchMess);
            delete hostmsg;
            EV << "Free Connection" << "\n";
        }else{
            int indexInGate = msg->getArrivalGate()->getIndex();
            cout << indexInGate;
            enb[indexInGate].push_back(hostmsg); // Insert to entrance buffer
            enb[indexInGate].front();

            int indexEnbCanForwardMsg = indexInGate;
            int minIndexId = hostmsg->getId();
            for (int i = 0; i < sizeofK - 1; i++){
                if (enb[i].size() > 0){
                    HostMessage *fontMsg = enb[i].front();
                    if (fontMsg->getId() < minIndexId){
                        indexEnbCanForwardMsg = i;
                        minIndexId = fontMsg->getId();
                    }
                }
            }

            if(avaiDesExb > 0){ // check if exit buffer is full or not

                avaiDesExb--;
                SwitchMessage *smsg = generateTransportMsg(indexEnbCanForwardMsg);
                scheduleAt(simTime().dbl() + periodTime, smsg);
            }
        }
    }else{
        SwitchMessage *smsg = check_and_cast<SwitchMessage *>(msg);

        if (smsg->getIsTransportMsg()){

            transportMessage(smsg->getIndexEnbTrans(), 3);
            delete smsg;

        }else if(smsg->getIsForwardMsg()) {

            int indexExbForwardMsg = smsg->getIndexExbForward();
            forwardMessage(exb[indexExbForwardMsg].front(), indexExbForwardMsg);
            delete smsg;
        }else if(smsg == delaySwitchMess){
            if (exb[3].size() > 0)
                forwardMessage(exb[3].front(), 3);
        }

    }

}

void Switch::forwardMessage(HostMessage *msg, int indexExb)
{

    // Forward message immediately
    if (!isBusyChannel && exb[indexExb].size() > 0){
        send(msg, "gate$o", indexExb);
        isBusyChannel = true;
        avaiDesExb++;
        scheduleAt(simTime().dbl() + timeDelayMsg , wakeUp);
        exb[indexExb].pop_front();

        int indexEnbCanTransport = -1;
        int idMsgCanTransport = numeric_limits<int>::max();
        for (int i = 0; i < enb.size(); i++){
            if (enb[i].size() > 0){
                if (enb[i].front()->getId() < idMsgCanTransport){
                    idMsgCanTransport = enb[i].front()->getId();
                    indexEnbCanTransport = i;
                }
            }
        }

        if (indexEnbCanTransport != -1){
            avaiDesExb--;
            SwitchMessage *smsg = generateTransportMsg(indexEnbCanTransport);
            scheduleAt(simTime().dbl() + periodTime, smsg);
        }
    }
}

void Switch::transportMessage(int indexEnb, int indexExb)
{
    if(exb[indexExb].size() < sizeofEXB){
        exb[indexExb].push_back(enb[indexEnb].front());
        enb[indexEnb].pop_front();

        SwitchMessage *forwardMsg = generateForwardMsg(indexExb); // Forward to destination node;
        scheduleAt(simTime().dbl() + periodTime, forwardMsg);

        cMessage *msg = new cMessage("CREDIT_DELAY_TIME");
        send(msg, "gate$o", indexEnb + sizeofK);
    }
}

SwitchMessage* Switch::generateTransportMsg(int indexEnbTrans)
{
    SwitchMessage *smsg = new SwitchMessage();
    smsg->setIndexEnbTrans(indexEnbTrans);
    smsg->setIsTransportMsg(true);
    smsg->setIsForwardMsg(false);
    smsg->setIndexExbForward(-1);
    return smsg;
}

SwitchMessage* Switch::generateForwardMsg(int indexExb)
{
    SwitchMessage *msg = new SwitchMessage();
    msg->setIndexEnbTrans(-1);
    msg->setIsTransportMsg(false);
    msg->setIsForwardMsg(true);
    msg->setIndexExbForward(indexExb);

    return msg;
}

void Switch::finish()
{

}

