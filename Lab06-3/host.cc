/*
 * host.cc
 *
 *  Created on: May 7, 2020
 *      Author: Admin
 */
#include<omnetpp.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include "myMessage_m.h"

using namespace std;
using namespace omnetpp;




// Source queue
struct SQ {
    int idHead = 0;
    int idTail = 0;

    int getHeader(){
        int ans = -1;
        if (idHead > 0){
            ans = idHead;
            if (idTail == 0){
                idHead = 0;
            }
            idHead++;
            idTail--;
        }
        return ans;
    }
};

// Exit buffer
struct EB {
    int *exitBuff;
    int avaiLength = 2;
};




class Host: public cSimpleModule {
private:
    // Source queue and Exit buffer in here
    SQ sq; // Source queue
    EB eb; // Exit buffer

    int counter_msg = 0;
    int counter = 0;

    // For check channel is busy or not
    bool isBusyChannel = false;

    int *msgForInterval;   // For destination node: count message in interval times;
    int lengthOfMsgForInterval;
    double timeForGenMess; // For source node: time for generate message
    double timeSimulation, intervalTime; // Time for simulation
    double timeDelayMsg = 0;

    HostMessage * genMsg;
    HostMessage * wakeUpMsg;
protected:
    void handleMessage(cMessage *msg) override;
    void initialize() override;
    void finish() override;
    HostMessage* generateMessage(int idMsg);

    void insertToSQ(int idMsg);
    void insertToEB();
    void sendMessageInEB();
};

Define_Module(Host);

void Host::initialize()
{
    // Get simulation's time
    timeSimulation = (double) getParentModule()->par("simulationTime") / 1000;
    intervalTime = (double) getParentModule()->par("intervalTime") / 1000;
    timeDelayMsg = (double) gate("gate$o")->getChannel()->par("delay");
    counter = (int) getParentModule()->par("sizeofEXB");
    eb.avaiLength = counter;


    isBusyChannel = false;
    if (strcmp(getName(), "hostDes") == 0){ // For destination node

        lengthOfMsgForInterval = (int)(timeSimulation / intervalTime);
        msgForInterval = (int *)calloc(lengthOfMsgForInterval, sizeof(int));

    }else{ // For begin node

        eb.exitBuff = (int *)calloc(eb.avaiLength, sizeof(int));
        timeForGenMess = (double)getParentModule()->par("cycleTimeGenMsg") / 1000;

        wakeUpMsg = new HostMessage();
        wakeUpMsg->setId(-1);

        // Generate a message
        insertToSQ(++counter_msg);

        // Generate a message for generate message after cycle generate message
        genMsg = new HostMessage();
        genMsg->setId(-1);
        scheduleAt(simTime().dbl() + timeForGenMess, genMsg);


    }
}

void Host::handleMessage(cMessage *msg)
{

    if (simTime().dbl() > timeSimulation) return;
    if(strcmp(msg->getName(), "CREDIT_DELAY_TIME") == 0){ // Signal from switch notify that entrance buffer had move a msg
        counter++;
        delete msg;
    }else{
        HostMessage * hostMsg = check_and_cast<HostMessage *>(msg);
        if (hostMsg == genMsg){ // Generate a new message
            insertToSQ(++counter_msg);
            scheduleAt(timeForGenMess + simTime().dbl(), hostMsg);
        }else {
            if (strcmp(getName(), "hostDes") == 0){ // Is destination;

               int indexTimeInterval = (simTime().dbl() / intervalTime - 1.0);
               msgForInterval[indexTimeInterval] += 1;
               EV << "a: " << msgForInterval[indexTimeInterval] << "\n";
               delete hostMsg;

               HostMessage *connFreeMsg = new HostMessage();
               connFreeMsg->setSignConnFree(true);
               send(connFreeMsg, "connectionFreeGate$o");

            }else if (hostMsg == wakeUpMsg){
               isBusyChannel = false;
               sendMessageInEB();
            }
        }
    }
}


HostMessage* Host::generateMessage(int idMsg)
{
    HostMessage* msg = new HostMessage();
    msg->setId(idMsg);

    return msg;
}


void Host::insertToSQ(int idMsg)
{
    if (sq.idHead == 0){
        sq.idHead = idMsg;
    }else{
        sq.idTail = idMsg;
    }

    insertToEB();
}

void Host::insertToEB()
{
    if (eb.avaiLength == 1){
        int idMsg = sq.getHeader();
        if(idMsg > 0){ // Insert to EB
            eb.exitBuff[1] = idMsg;
            eb.avaiLength--;
        }
    }else if (eb.avaiLength == 2){
        int idMsg = sq.getHeader();
        if (idMsg > 0){ // Insert to SB 0
            eb.exitBuff[0] = idMsg;
            eb.avaiLength--;
            int idSeMsg = sq.getHeader();
            if (idSeMsg > 0){
                eb.exitBuff[1] = idSeMsg;
                eb.avaiLength--;
            }
        }
    }
    sendMessageInEB();
}

void Host::sendMessageInEB()
{
    if (counter > 0 && eb.avaiLength < 2){ // Check if exists any message in Exit buffer
        if (!isBusyChannel){
            // Listener channel: is busy or not
            isBusyChannel = true;
            counter--;

            HostMessage *msg = generateMessage(eb.exitBuff[0]);
            send(msg, "gate$o");

            scheduleAt(simTime().dbl() + timeDelayMsg, wakeUpMsg);

            eb.exitBuff[0] = eb.exitBuff[1];
            eb.exitBuff[1] = 0;
            eb.avaiLength++;
        }
    }
}

void Host::finish()
{
    if (strcmp(getName(), "hostDes") == 0)
    {

        int count_msg = 0;
        for (int i = 0; i < lengthOfMsgForInterval; i++){
            EV << "Interval Time " << (i + 1) << " : " << msgForInterval[i] << "\n";
            count_msg += msgForInterval[i];
        }
        EV << "Interval Time: " << intervalTime << "\n";
        EV << "Number of Msg: " << count_msg << "\n";
        EV << "Number of msg per intervalTime in simulation time: " << (double)count_msg * timeSimulation / lengthOfMsgForInterval;
    }

}
